FROM opensuse/leap:15.2

EXPOSE 80 443 22 24

COPY baseline /baseline
RUN /baseline/setup-repo.sh
RUN /baseline/install-packages.sh
RUN /baseline/postinstall.sh

COPY preflight /preflight
RUN /preflight/setup.sh
CMD ["/bin/bash", "/app/init.sh"]
