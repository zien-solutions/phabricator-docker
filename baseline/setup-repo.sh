#!/bin/bash

set -e
set -x

# Add repositories
zypper --non-interactive ar https://mirrorcache.opensuse.org/repositories/devel:languages:php/openSUSE_Leap_15.2/ php
zypper --non-interactive ar https://mirrorcache.opensuse.org/repositories/devel:languages:nodejs/openSUSE_Leap_15.2/ nodejs
# --- leap 15.2 has nodejs and python available by default?
# zypper --non-interactive ar http://download.opensuse.org/repositories/home:/marec2000:/nodejs/openSUSE_Leap_42.3/ nodejs
# zypper --non-interactive ar http://download.opensuse.org/repositories/devel:/languages:/python/openSUSE_Leap_42.3/ python

# Add SCM package for Git, Subversion, Mercurial...
zypper --non-interactive ar https://mirrorcache.opensuse.org/repositories/devel:tools:scm/openSUSE_Leap_15.2/ scm

# Refresh metadata
zypper --gpg-auto-import-keys --non-interactive refresh
