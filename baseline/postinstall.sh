#!/bin/bash

set -e
set -x

# Create users and groups
echo "nginx:x:497:495:user for nginx:/var/lib/nginx:/bin/false" >> /etc/passwd
echo "nginx:x:495:" >> /etc/group
echo "PHABRICATOR:x:2000:2000:user for phabricator:/srv/phabricator:/bin/bash" >> /etc/passwd
echo "wwwgrp-phabricator:x:2000:nginx" >> /etc/group
echo "PHABRICATOR:*:18987::::::" >> /etc/shadow

# Set up the Phabricator code base
mkdir /srv/phabricator
chown PHABRICATOR:wwwgrp-phabricator /srv/phabricator
cd /srv/phabricator
sudo -u PHABRICATOR git clone https://www.github.com/phacility/arcanist.git /srv/phabricator/arcanist
sudo -u PHABRICATOR git clone https://www.github.com/phacility/phabricator.git /srv/phabricator/phabricator
sudo -u PHABRICATOR git clone https://www.github.com/PHPOffice/PHPExcel.git /srv/phabricator/PHPExcel
cd /

# --- certbot-auto is deprecated (we aren't going to use it anyway)
# Clone Let's Encrypt
#git clone https://github.com/letsencrypt/letsencrypt /srv/letsencrypt
#cd /srv/letsencrypt
#./letsencrypt-auto-source/letsencrypt-auto --help
#cd /
